 # Matemáticas

##  Conjuntos, aplicaciones y funciones (2020)

```plantuml
@startmindmap
<style>
 node {
  
  BackgroundColor #FFFE9B
  LineColor #0F0F0F
 }
 arrow{
  LineColor black
 }
 </style>
 *[#F9E3DD]  <b>Conjuntos, aplicaciones y funciones (2020)
  *[#A9F6F0] Conjuntos
   * Inclusión de conjuntos.
    * Un conjunto este contenido
     *_ En
      * Otro si un todo elemento del primer conjunto\n pertenece al otro conjunto.
    *_ Operaciones
     * Interseccion.
      * Elementos que pertenecen simultáneamente a ambos.
     * Unión.
      * Elementos que pertenecen al menos a alguno de ellos.
     * Complementación de conjuntos.
      * Complementario de un conjunto.
       *_ Que
        * No pertenece a un conjunto dado. 

   * Tipos de conjuntos
    *[#A9F6A9] Universal.
     * Conjunto de referencia en el\n que ocurre toda la teoría. 
    *[#A9F6A9] Vacío.
     * Necesidad lógica.
      *_ Para
       * Cerrar el conjunto que no tiene elemento.
   *_ Representación
    *[#F1A9F6] Diagramas de Venn. 
     * Recurso didáctico más característicos\n de la inclusión de conjuntos.
     *_ creado por un lógico llamado
      * Jonh Venn.
     * Ayudan a comprender intuitivamente\n la posición de las operaciones.
      *_ Los cuales son
       * Intersección.
       * Unión.
       * Complementario.

     * No sirve como método de demostración.

   * Cardinal de un conjunto
    * El número de elementos que\n conforman el conjutno.
    * Es un número natural.

    *_ Propiedades
     * Fórmula 
      * Cardinal de la unión de conjuntos.
       *_ El cardinal de una unión es
        * Igual al cardinal de uno de los conjuntos,\n mas el cardinal del segundo conjunto menos el cardinal\n de la intersección.

   * Acotación de cardinales. 
    *	Basada en la fórmula cardinal\n de unión de conjuntos 
     *_ Para
      * Encontrar una serie de relaciones.
       *_ Sobre
        * Quien es mayor, menor o igual entre\n cardinales de conjuntos.

  *[#A9F6F0] Aplicaciones y funciones.
   * Cualquier diciplina científica\n estudiada las transformaciones\n el concepto matemático\n de transformación o cambio
    *_ Se denomina
     * Aplicación o función.  
      *[#D6B5F3] Aplicación.
       * Es una transformación o regla.
        *_ Que convierte a
         * Cada uno de los elementos de un\n determinado conjunto inicial
          *_ En
           * Un único elemento de un conjunto final.
          * Imagen o imagen inversa de un subconjunto.

          *_ Tipos de aplicaciones
           * Aplicación inyectiva.
           * Aplicación sobreyectiva.
           * Aplicación biyectiva.

          * Transformación de transformación.
           * Una aplicación primero y\n otra aplicación a continuación mediante\n la primera aplicación.
            *_ Se llama
             * Composición de aplicaciones. 
          
      *[#D6B5F3] Función        
       * Cuando se tiene una transformación\n o aplicación entre conjuntos de números,\n se utiliza la palabra función.
        * Particularidad
         * Conjuntos que se transforman\n son conjuntos de números. 
      
        * Grafica de la función.
         * Sistema de coordenadas.
          *_ En el cual
           * Se obtiene una figura.
         * Serie de puntos.
          *_ En el cual
           * se obtienen conjunto de los puntos cuya\n primera coordenada del plano es el elemento del primer\n conjunto y su segunda coordenada es el\n transformado mediante ese conjunto.
	
@endmindmap
```
## Funciones (2010)

```plantuml
@startmindmap
<style>
 node {
  
  BackgroundColor #FFFE9B
  LineColor #0F0F0F
 }
 arrow{
  LineColor black
 }
 </style>
*[#F9E3DD]  <b>Funciones (2010)
 * Es el corazón de las matemáticas. 
  *[#B5F3B7] Función
   * Es una aplicación especial.
    * Un conjunto que esta relacionado con\n conjuntos de números.
     *_ Mas en particular 
      * Números reales. 
 *_ Representación grafica.
  * Cartesiana
   * Puede depender del cambio de la función.
	 * Es un plano en el eje X y Y de los números reales.  
	  * Tomando un número para la F(x)\n y llegar a Y. Para sacar una coordenada.
 *_ Características 
  * Crecientes.
   * Cuando las variables del primer\n conjunto aumentan sus imágenes. 
  * Decrecientes.
   * Cuando al aumentar las x\n disminuyen las imágenes.
    * Cada vez sus números son más pequeños. 

  * Máximos y mínimos relativos.
   * Aumentamos los valores de la función. 
    * Máximos relativos.
     * Si la función decrece.
    *	Mínimos relativos.
     * Si la función crece.
  * Limite.
   * Funciones que en un momento\n determinado tienen saltos.
   * Carece de límite cuando tiende a 0. 
  * Continuidad.
   *_ Es 
    * Una función que no produce\n saltos ni discontinuidad. 
     *_ Son 
      * Funciones más manejables. 

  * Derivada
   * Es un tipo de transformación.
    *_ Ya que 
     * Transforma los números a través\n de una función dada. 
   *_ Motivación de la derivada
    * Resolver el problema de la aproximación\n de una función compleja
     *_ Mediante 
      * Una función\n lineal simple. 


@endmindmap
```

## La matemática del computador (2002)

```plantuml
@startmindmap
<style>
 node {
  
  BackgroundColor #FFFE9B
  LineColor #0F0F0F
 }
 arrow{
  LineColor black
 }
 </style>
*[#F9E3DD]  <b>La matemática del computador (2002)
 *_ Utiliza 
  * Aritmética finita.
   *_ Tiene
    * Limitaciones en los medios físicos.
     *_ No se puede
      * Representar los números\n con infinitos decimales.
    *_ Solo tiene
     * Números finitos de posiciones.
      *_ Donde
       * Convertir 0 y 1 en una serie\n de posiciones de memoria.
 *_ Basado en
  * El sistema binario.
   *_ Utiliza 
    * Aritmética binaria.
   *_ Es un 
    * Sistema simple con 2 posiciones. 
     * Enlazado con la lógica booleana.
      *_ Con 
       * La pertenencia y\n no pertenecía en conjuntos. 
  
   * Se construye un sistema de numeración. 
    * Se pasa corriente para 1. 
    * Se impide corriente para 0. 
   * Sistema octal y hexadecimal. 
    * Representaciones compactas de\n números empleadas en informática. 

 * Truncamiento y redondeo.
  * Cuando el número que tratamos\n de representar tiene muchas\n cifras, enteras o decimales.
   *_ Se tiene que
    * Acortar la representación de ese número. 
     * Despreciarlas y retocar la\n última cifra que no\n despreciamos de un modo adecuado.
      *_ Para
       * Intentar cometer el menor error posible.

 * Codificación.
  *_ Para poder
   * Sacar representaciones de\n los símbolos como letras y números.  
   
   * Números enteros.
    *_ Con representación 
     * Magnitud signo.
	  * En exceso.
	  * En complemento 2.
    * Para poder representar numeros\n muy grandes o muy pequeños
     *_ Se usa
      * Notación científica. 
       * En forma del producto de un\n número con una potencia de diez. 
       *_ Se realizan conversiones
        * Acortando las cifras en binario.
         *_	Sistema binario
          * Octal. 
          * Hexadecimal. 



@endmindmap
```
